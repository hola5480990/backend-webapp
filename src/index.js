var http = require("http");

var {countries}=require("countries-list");



var server = http.createServer(function (request, response) {
  if (request.url === "/") {
    response.writeHead(200, { "Content-Type": "text/html" });
    response.write("<html><body><p>pagina de inicio</p></body></html>");
    response.end();
  } else if (request.url === "/exit") {
    response.writeHead(200, { "Content-Type": "text/html" });
    response.write("<html><body><p>pagina de salida</p></body></html>");
    response.end();
  } else if (request.url === "/country") {
    response.writeHead(200, { "Content-Type": "application/json" });
    response.write(JSON.stringify(countries.EC));
    response.end();
  } else {
    response.writeHead(404, { "Content-Type": "text/html" });
    response.write("<html><body><p>Not Found</p></body></html>");
    response.end();
  }
});

server.listen(4000);

console.log("runing on 4000");
